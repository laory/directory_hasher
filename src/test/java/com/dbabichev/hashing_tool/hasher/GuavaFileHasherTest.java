package com.dbabichev.hashing_tool.hasher;

import org.junit.Test;

import java.io.File;

import static com.dbabichev.hashing_tool.TestUtil.getResourceFile;
import static org.junit.Assert.assertEquals;

/**
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 11:20 AM
 */
public class GuavaFileHasherTest {

    private GuavaFileHasher hasher = new GuavaFileHasher();

    @Test
    public void hash() throws Exception {
        final File file = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/hello_world.file");

        final String hash = hasher.hash(file);

        assertEquals("Hash doesn't work properly",
                "309ecc489c12d6eb4cc40f50c902f2b4d0ed77ee511a7c7a9bcd3ca86d4cd86f989dd35bc5ff499670da34255b45b0cfd830e81f605dcf7dc5542e93ae9cd76f", hash);
    }

    @Test
    public void hashIndependent() throws Exception {
        final File file = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/hello_world.file");

        final String expectedHash = hasher.hashString("hello world");
        final String actualHash = hasher.hash(file);

        assertEquals("Hash doesn't work properly", expectedHash, actualHash);
    }

    @Test
    public void hashMultiline() throws Exception {
        final File file = getResourceFile("com/dbabichev/hashing_tool/hasher/sha512.txt");

        final String hash = hasher.hash(file);

        assertEquals("Hash doesn't work properly",
                "5138c7e352e47b5c09e976ef882c5ead88bfc2a3d0269583b49990ae1dda9cc1ee3472694d4c40000ce03785d2b223f803a2974474ee7d4bae048282fa7df428", hash);
    }

    @Test
    public void hashDistinct() throws Exception {
        final File file1 = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/hello_world.file");
        final File file2 = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/hello_world2.file");

        final String hash1 = hasher.hash(file1);
        final String hash2 = hasher.hash(file2);

        assertEquals("Hash doesn't return distinct values", hash1, hash2);
    }

    @Test
    public void hashIdempotent() throws Exception {
        final File file1 = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/hello_world.file");

        final String hash1 = hasher.hash(file1);
        final String hash2 = hasher.hash(file1);

        assertEquals("Hash isn't idempotent", hash1, hash2);
    }

    @Test
    public void hashEmptyFile() throws Exception {
        final File file = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/empty.file");

        final String hash = hasher.hash(file);

        assertEquals("Hash doesn't work properly",
                "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e", hash);
    }

    @Test
    public void suppliedInput() throws Exception {
        assertEquals("Hash doesn't work properly",
                "af371785c4fecf30acdd648a7d4d649901eeb67536206a9f517768f0851c0a06616f724b2a194e7bc0a762636c55fc34e0fcaf32f1e852682b2b07a9d7b7a9f9",
                hasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/bar/fileA.dat")));
        assertEquals("Hash doesn't work properly",
                "46868d0a185e942d2fd15739b60096feab4ccdc99139cca4c9db82325606115c8803a6bffe37d6e54c791330add6e1fc861bfa79399f01cc88eed3fcedce13d4",
                hasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/bar/fileB.dat")));
        assertEquals("Hash doesn't work properly",
                "c1e42aa0c8908c9c3d49879a4fc04a59a755735418ddc3a200e911673da188bf46f67818972eac54b38422895391c82b2b0e0cf34aea9468c3ad73c2d0ffa912",
                hasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/bar/fileC.dat")));
        assertEquals("Hash doesn't work properly",
                "9dd88c920d86ac24112eb692e87b047bb6e69cd413593b009af62a29a71daa68f094dd3340976ae9b8e5d8e5d66d964179409c049103f91f3ccba80d9de63b7a",
                hasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/faz/fileD.dat")));
        assertEquals("Hash doesn't work properly",
                "40c9964826072dbebe00ea99db34a8c8268088738de8d2a9c02743d0eed36a018adf122bacd789cc569ba2f5f54c75191683e3f252486bf71a5824ae99e20017",
                hasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/faz/fileE.dat")));
    }
}