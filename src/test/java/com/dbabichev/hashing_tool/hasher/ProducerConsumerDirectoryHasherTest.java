package com.dbabichev.hashing_tool.hasher;

import org.junit.Test;

import java.io.File;

import static com.dbabichev.hashing_tool.TestUtil.getEmptyDirFile;
import static com.dbabichev.hashing_tool.TestUtil.getResourceFile;
import static org.junit.Assert.assertEquals;

/**
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 12:53 PM
 */
public class ProducerConsumerDirectoryHasherTest {

    private final Hasher fileHasher = new GuavaFileHasher();
    private final Hasher singleThreadHasher = new SingleThreadDirectoryHasher(fileHasher);
    private final Hasher producerConsumerHasher = new ProducerConsumerDirectoryHasher(fileHasher, ProducerConsumerDirectoryHasher.Mode.HASHTHROUGH);

    @Test
    public void hash() throws Exception {
        final File dir = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/hello_world_dir");

        final String expectedHash = singleThreadHasher.hash(dir);
        final String actualHash = producerConsumerHasher.hash(dir);

        assertEquals("Hash doesn't work properly", expectedHash, actualHash);
    }

    @Test
    public void hashDeep() throws Exception {
        final File dir = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change");

        final String expectedHash = singleThreadHasher.hash(dir);
        final String actualHash = producerConsumerHasher.hash(dir);

        assertEquals("Hash doesn't work properly", expectedHash, actualHash);
    }

    @Test
    public void hashEmptyDir() {
        final File emptyDir = getEmptyDirFile();

        final String expectedHash = singleThreadHasher.hash(emptyDir);
        final String actualHash = producerConsumerHasher.hash(emptyDir);

        assertEquals("Hash doesn't work properly", expectedHash, actualHash);
    }

    @Test
    public void multilineFile() throws Exception {
        final File file = getResourceFile("com/dbabichev/hashing_tool/hasher/sha512.txt");
        assertEquals(fileHasher.hash(file),
                producerConsumerHasher.hash(file));
        assertEquals(fileHasher.hash(file),
                producerConsumerHasher.hashBytes(fileHasher.readBytes(file)));
    }

    @Test
    public void suppliedInput() throws Exception {
        assertEquals("6ef01eac687a58a3b28d924f3fa0641b7629356dfca436beb457424d649d4a64faf60b228c3738a3c75da49052264c92135f8aa296cdaad0d4800a7496f88e62",
                producerConsumerHasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/bar")));
        assertEquals("9f0c752149eb2699f077215798e03f16837ec85fda55a57efeee6480e8ee43971092deec7ff553476d53f0760d637d41b2c31be2b4ef55614ab5d17ab0f8f6dc",
                producerConsumerHasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/faz")));
        assertEquals("6dd415b8f89a52dd3ce277946150f1df6ea98a89296d0574db69b1fbc4d0aade51abba041529309abfbf07897808edb31a4a6b73a9b7c79fce20476062f6288a",
                producerConsumerHasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input")));
    }
}