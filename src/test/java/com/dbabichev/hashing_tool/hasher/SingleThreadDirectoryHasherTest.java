package com.dbabichev.hashing_tool.hasher;

import org.junit.Test;

import java.io.File;

import static com.dbabichev.hashing_tool.TestUtil.getEmptyDirFile;
import static com.dbabichev.hashing_tool.TestUtil.getResourceFile;
import static org.junit.Assert.assertEquals;

/**
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 12:53 PM
 */
public class SingleThreadDirectoryHasherTest {

    private final Hasher fileHasher = new GuavaFileHasher();
    private final Hasher directoryHasher = new SingleThreadDirectoryHasher(fileHasher);

    @Test
    public void hash() throws Exception {
        final File dir = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change/hello_world_dir");

        final String actualHash = directoryHasher.hash(dir);

        assertEquals("Hash doesn't work properly",
                "f95b1e9b2f29ec36bbe6bd8ba177357527be65fa7ce682991732b7ae52cf6d9960ee553808215ac92b8d639984b6c3874ee5f71dbfa7ffcdd9074ed61259ff34", actualHash);
    }

    @Test
    public void hashDeep() throws Exception {
        final File dir = getResourceFile("com/dbabichev/hashing_tool/hasher/test_files_do_not_change");

        final String actualHash = directoryHasher.hash(dir);

        assertEquals("Hash doesn't work properly",
                "de32f421809a4f573844dcc8297207da45436bfe88cc047db035ecad477902c55a6d42e6bfb938094825ff8850d5f74b35c64527bcdc736ef7cd1d3efcfcf973", actualHash);
    }

    @Test
    public void hashEmptyDir() {
        final File emptyDir = getEmptyDirFile();

        final String actualHash = directoryHasher.hash(emptyDir);

        assertEquals("Hash doesn't work properly",
                "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e", actualHash);
    }

    @Test
    public void multilineFile() throws Exception {
        final File file = getResourceFile("com/dbabichev/hashing_tool/hasher/sha512.txt");

        final String expectedHash = "5138c7e352e47b5c09e976ef882c5ead88bfc2a3d0269583b49990ae1dda9cc1ee3472694d4c40000ce03785d2b223f803a2974474ee7d4bae048282fa7df428";
        assertEquals(expectedHash,
                fileHasher.hash(file));
        assertEquals(expectedHash,
                fileHasher.hashBytes(fileHasher.readBytes(file)));

    }

    @Test
    public void suppliedInput() throws Exception {
        assertEquals("6ef01eac687a58a3b28d924f3fa0641b7629356dfca436beb457424d649d4a64faf60b228c3738a3c75da49052264c92135f8aa296cdaad0d4800a7496f88e62",
                directoryHasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/bar")));
        assertEquals("9f0c752149eb2699f077215798e03f16837ec85fda55a57efeee6480e8ee43971092deec7ff553476d53f0760d637d41b2c31be2b4ef55614ab5d17ab0f8f6dc",
                directoryHasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input/faz")));
        assertEquals("6dd415b8f89a52dd3ce277946150f1df6ea98a89296d0574db69b1fbc4d0aade51abba041529309abfbf07897808edb31a4a6b73a9b7c79fce20476062f6288a",
                directoryHasher.hash(getResourceFile("com/dbabichev/hashing_tool/hasher/input")));
    }
}