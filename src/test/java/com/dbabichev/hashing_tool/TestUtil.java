package com.dbabichev.hashing_tool;

import java.io.File;

/**
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 12:57 PM
 */
public class TestUtil {
    public static File getResourceFile(final String path) {
        return new File(TestUtil.class.getClassLoader().getResource(path).getFile());
    }

    public static File getEmptyDirFile() {
        final File emptyDir = new File("empty_dir");
        emptyDir.mkdir();
        emptyDir.deleteOnExit();
        return emptyDir;
    }
}
