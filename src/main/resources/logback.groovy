appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = '%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} - %msg%n'
    }
}

if (Boolean.valueOf(System.getProperty("verbose"))) {
    logger("com.dbabichev.hashing_tool.hasher", DEBUG, ["STDOUT"], false)
    root(DEBUG, ["STDOUT"])
}