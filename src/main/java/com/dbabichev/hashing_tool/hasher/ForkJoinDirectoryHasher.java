package com.dbabichev.hashing_tool.hasher;

import java.io.File;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import static java.util.concurrent.ForkJoinTask.invokeAll;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * Implementation of {@link DirectoryHasher} which defines a strategy for hashing a directory.
 * <p>
 * Hashing is done by multiple threads in work-stealing fashion using {@link ForkJoinPool} with specified number of {@link #threads}.
 * <p>
 * It is recommended to use this version <b>only</b> when reading from file is extremely-fast, which is not the case most of the
 * time. Otherwise, the performance will get even worse than using {@link SingleThreadDirectoryHasher}
 * <p>
 * Every hashing operation happens in separate {@link HashingRecursiveTask}. To compose a hash of a directory all internal
 * files are hashed in their own {@link HashingRecursiveTask}s which are then joined to retrieve internal hashes, concatenated
 * and hashed with {@link #hashString(String)}
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 12:16 PM
 */
public class ForkJoinDirectoryHasher extends DirectoryHasher {

    private final ForkJoinPool forkJoinPool;
    private final int threads;

    public ForkJoinDirectoryHasher(final Hasher fileHasher, final int threads) {
        super(fileHasher);
        this.threads = threads;
        forkJoinPool = new ForkJoinPool(threads);
    }

    /**
     * An algorithm for hashing <u>directories</u> or particular files. In case when {@code file}
     * is directory, {@link #hashDirectory(File)} is called inside of a {@link HashingRecursiveTask}. Otherwise,
     * if {@code file} is a regular file, {@link #hashFile(File)} is also called inside of its own {@link HashingRecursiveTask}.
     *
     * @param file to be hashed
     * @return {@link String} representation of hexed hash of {@link File} content
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    @Override
    public String hash(final File file) {
        final HashingRecursiveTask hashingRecursiveTask = new HashingRecursiveTask(file);
        return forkJoinPool.invoke(hashingRecursiveTask);
    }

    /**
     * Method recursively creates {@link HashingRecursiveTask} for every file in the directory, invokes tasks on current pool,
     * join on every task in order to get all internal hashes, concatenates them and hashes the resulting string
     *
     * @param file directory to be hashed
     * @return {@link String} representation of hexed hash of {@link File} content
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    @Override
    protected String hashDirectory(final File file) {
        final List<HashingRecursiveTask> computeHashTasks = getInternalFilesSortedByName(file).stream()
                .map(HashingRecursiveTask::new)
                .collect(toList());
        final String concatenatedInternalHashes = invokeAll(computeHashTasks).stream()
                .map(ForkJoinTask::join)
                .collect(joining());

        return fileHasher.hashString(concatenatedInternalHashes);
    }

    private class HashingRecursiveTask extends RecursiveTask<String> {

        private final File file;

        private HashingRecursiveTask(final File file) {
            if (file == null) throw new HashingException("File should not be null");
            this.file = file;
        }

        @Override
        protected String compute() {
            return ForkJoinDirectoryHasher.super.hash(file);
        }
    }
}
