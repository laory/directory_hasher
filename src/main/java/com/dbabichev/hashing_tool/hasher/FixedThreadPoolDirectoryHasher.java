package com.dbabichev.hashing_tool.hasher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

/**
 * Implementation of {@link AsyncDirectoryHasher} which defines a strategy for hashing a directory.
 * <p>
 * Hashing is done by multiple threads using {@link CompletableFuture} with specified {@link ExecutorService} of
 * fixed {@link @queueCapacity}. The number of threads which serve an {@link ExecutorService} is specified in {@link #threads}.
 * <p>
 * It is recommended to use this version when file tree under a given root file is not very deep and internal files are
 * mostly of medium size.
 * <p>
 * Every hashing operation happens in separate {@link CompletableFuture}. To compose a hash of a directory all internal
 * files are hashed in their own {@link CompletableFuture}s which are composed into a {@link CompletableFuture} which
 * is done when <b>all of them</b> are done.
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 12:16 PM
 */
public class FixedThreadPoolDirectoryHasher extends AsyncDirectoryHasher {

    public static final int DEFAULT_THREADS = Runtime.getRuntime().availableProcessors();
    public static final int DEFAULT_QUEUE_CAPACITY = 2048;

    private static final Logger LOG = LoggerFactory.getLogger(FixedThreadPoolDirectoryHasher.class);

    public FixedThreadPoolDirectoryHasher(final Hasher fileHasher) {
        this(fileHasher, DEFAULT_THREADS, DEFAULT_QUEUE_CAPACITY);
    }

    public FixedThreadPoolDirectoryHasher(final Hasher fileHasher, final int threads, final int queueCapacity) {
        super(threads, queueCapacity, fileHasher);
    }

    /**
     * Implementation of an algorithm for hashing <u>directories</u> or particular files.
     * <p>
     * Asynchronously hashes files and directories in a file tree under the root. Any subdirectory produces more async
     * hashing tasks to be executed and then composed.
     *
     * @param root file or directory to be hashed
     * @return {@link String} representation of hexed hash of {@link File} contents
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    @Override
    public String hash(final File root) {
        validate(root);
        if (!root.isFile() && !root.isDirectory()) throw new HashingException("You can hash only directory or file");
        final ExecutorService executor = initThreadPoolExecutor();
        try {
            final CompletableFuture<String> futureHash = hashAsync(root, executor);
            return futureHash.get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new HashingException("Unable to hash a file " + root, e);
        } catch (ExecutionException e) {
            throw new HashingException("Unable to hash a file " + root, e);
        } finally {
            shutdownAndAwaitTermination(executor);
        }
    }

    private CompletableFuture<String> hashAsync(final File file, final ExecutorService executor) {
        if (file.isFile()) {
            return hashFileAsync(file, executor);
        }
        return hashDirectoryAsync(file, executor);
    }

    private CompletableFuture<String> hashFileAsync(final File file, final ExecutorService executor) {
        return CompletableFuture.supplyAsync(() -> fileHasher.hash(file), executor);
    }

    private CompletableFuture<String> hashDirectoryAsync(final File root, final ExecutorService executor) {
        return getInternalFiles(root, executor)
                .thenApply(internalFiles -> internalFiles.stream()
                        .map(internalFile -> hashAsync(internalFile, executor))
                        .collect(Collectors.toList())
                )
                .thenCompose(internalHashes -> {
                    final CompletableFuture<String> directoryHash = allOf(internalHashes).thenApply(hashes -> {
                        final String concatenatedHashes = hashes.stream().collect(joining());
                        final String hash = hashString(concatenatedHashes);
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Hash of directory {} - {}", root, hash);
                        }
                        return hash;
                    });
                    return directoryHash;
                });
    }

    private <T> CompletableFuture<List<T>> allOf(final List<CompletableFuture<T>> futures) {
        final CompletableFuture[] hashesArr = futures.toArray(new CompletableFuture[futures.size()]);
        return CompletableFuture.allOf(hashesArr)
                .thenApply(v -> futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList()));
    }

    private CompletableFuture<List<File>> getInternalFiles(final File root, final ExecutorService executor) {
        return CompletableFuture.supplyAsync(() -> getInternalFilesSortedByName(root), executor);
    }

}
