package com.dbabichev.hashing_tool.hasher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Abstract implementation of {@link Hasher} interface in order to set up a basic algorithm in method {@link #hash(File)} for
 * hashing <u>directories</u> by utilizing supplied {@link #fileHasher} for hashing particular {@link File}s from those
 * directories.
 * <p>
 * The algorithm is based on checking whether given {@link File} is a file or directory and calling one of
 * corresponding methods {@link #hashFile(File)} or {@link #hashDirectory(File)} which is a template method.
 * <p>
 * The hash of a file is a {@link #hashFile(File)} of its contents.
 * The hash of a directory is generated by {@link #hashDirectory(File)}.
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 12:34 PM
 */
public abstract class DirectoryHasher implements Hasher {

    private static final Logger LOG = LoggerFactory.getLogger(DirectoryHasher.class);

    /**
     * Common {@link Comparator} which is used for sorting files within directory
     */
    protected static final Comparator<File> BY_FILE_NAME = Comparator.comparing(File::getName);

    /**
     * Hasher which is used for hashing {@link File}s whose {@link File#isFile()} return {@literal true}
     */
    protected final Hasher fileHasher;

    /**
     * Constructs a new {@link DirectoryHasher} which can hash a directory or a particular file with help of {@link #fileHasher}
     *
     * @param fileHasher which is used for hashing files within directories
     */
    protected DirectoryHasher(final Hasher fileHasher) {
        this.fileHasher = fileHasher;
    }

    /**
     * An algorithm for hashing <u>directories</u> or particular files. In case when {@code file}
     * is directory, the template method {@link #hashDirectory(File)} is called. Otherwise, if {@code file} is a regular
     * file, {@link #hashFile(File)} is called.
     *
     * @param file to be hashed
     * @return {@link String} representation of hexed hash of {@link File} content
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    @Override
    public String hash(final File file) {
        validate(file);
        if (!file.isFile() && !file.isDirectory()) throw new HashingException("You can hash only directory or file");
        if (file.isFile()) {
            return hashFile(file);
        }
        final String hash = hashDirectory(file);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Hash of directory {} - {}", file, hash);
        }
        return hash;
    }

    /**
     * Delegates call to {@link @fileHasher}
     *
     * @param string to be hashed
     * @return {@link String} representation of hexed hash of a string
     * @throws HashingException in case if given string can not be hashed due to internal issue
     */
    @Override
    public String hashString(final String string) {
        return fileHasher.hashString(string);
    }

    /**
     * Delegates call to {@link @fileHasher}
     *
     * @param bytes array to be hashed
     * @return {@link String} representation of hexed hash of a byte array
     * @throws HashingException in case if given byte array can not be hashed due to internal issue
     */
    @Override
    public String hashBytes(final byte[] bytes) {
        return fileHasher.hashBytes(bytes);
    }

    /**
     * Delegates call to {@link @fileHasher}
     *
     * @param file to be read as bytes
     * @return byte array of file's content
     * @throws HashingException in case if given {@link File} can not be read as bytes due to internal issue
     */
    @Override
    public byte[] readBytes(final File file) {
        return fileHasher.readBytes(file);
    }

    /**
     * Delegates call to {@link @fileHasher} for hashing a particular {@link File} whose {@link File#isFile()} returns {@code true}.
     * <p>
     * The hash of a file is a hash of its contents then hexed encoded.
     *
     * @param file to be hashed
     * @return {@link String} representation of hexed hash of {@link File} content
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    protected String hashFile(final File file) {
        return fileHasher.hash(file);
    }

    /**
     * Template method for hashing a {@link File} whose {@link File#isDirectory()} returns {@code true}.
     * <p>
     * The hash of a directory is generated by hashing each file or subdirectory in alphabetical order, then
     * concatenating the hashed value, then applying {@link #hashString(String)} to the result
     *
     * @param file directory to be hashed
     * @return {@link String} representation of hexed hash of a directory
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    protected abstract String hashDirectory(final File file);

    /**
     * Method returns internal files of given directory which are sorted with {@link #BY_FILE_NAME} comparator.
     *
     * @param root directory whose internal files need to be returned
     * @return list of files from given directory sorted with {@link #BY_FILE_NAME} comparator
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    protected List<File> getInternalFilesSortedByName(final File root) {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(root.getPath()))) {
            final List<File> internalFiles = StreamSupport.stream(directoryStream.spliterator(), false)
                    .map(Path::toFile)
                    .sorted(BY_FILE_NAME)
                    .collect(Collectors.toList());
            return internalFiles;
        } catch (IOException e) {
            throw new HashingException("Unable to hash a directory " + root, e);
        }
    }
}
