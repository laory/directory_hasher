package com.dbabichev.hashing_tool.hasher;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * File hasher which uses Guava library as an implementation of SHA-512 algorithm. The main advantage of provided
 * implementation is that it doesn't need the whole file in memory for hashing. Since SHA-512 needs only a piece of
 * data per iteration it is efficient to perform buffered reading of file and hash a buffer at a time.
 *
 * This class can be used by other implementations of {@link DirectoryHasher}
 *
 * Helper methods: {@link #hashString(String)}, {@link #hashBytes(byte[])}, {@link #readBytes(File)} are also backed by
 * Guava library.
 *
 * User: Dmytro_Babichev
 * Date: 9/20/2017
 * Time: 6:58 PM
 */
public class GuavaFileHasher implements Hasher {

    private static final Logger LOG = LoggerFactory.getLogger(GuavaFileHasher.class);

    /**
     * Method calculates a hash of a {@link File} and transforms it into a string representation of a hex value.
     * <p>
     * The hash of a file is a hash of its contents then hexed encoded.
     *
     * Implementation doesn't read the whole file in memory, thus is efficient from memory consumption and performance
     * perspective.
     *
     * @param file to be hashed
     * @return {@link String} representation of hexed hash of {@link File} content
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    @Override
    public String hash(final File file) {
        validate(file);
        if (!file.isFile()) throw new HashingException("You can hash only a file");
        try {
            final String hash = Files.asByteSource(file).hash(getHashFunction()).toString();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Hash of {} - {}", file, hash);
            }
            return hash;
        } catch (IOException e) {
            throw new HashingException("Unable to hash a file " + file, e);
        }
    }

    @Override
    public String hashString(final String text) {
        return getHashFunction()
                .hashString(text, StandardCharsets.UTF_8)
                .toString();
    }

    @Override
    public String hashBytes(final byte[] bytes) {
        return getHashFunction()
                .hashBytes(bytes)
                .toString();
    }

    @Override
    public byte[] readBytes(final File file) {
        try (final FileInputStream fileInputStream = new FileInputStream(file)) {
            return ByteStreams.toByteArray(fileInputStream);
        } catch (IOException e) {
            throw new HashingException("Unable to read a file " + file, e);
        }
    }

    private HashFunction getHashFunction() {
        return Hashing.sha512();
    }
}
