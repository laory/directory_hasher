package com.dbabichev.hashing_tool.hasher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;

/**
 * Implementation of {@link AsyncDirectoryHasher} which defines a strategy for hashing a directory.
 * <p>
 * Hashing is done by multiple threads according to Producer/Consumer pattern. All Tasks are held in a {@link #taskQueue}
 * with fixed {@link @queueCapacity}. The number of threads is specified in {@link #threads}.
 * Intermediate results are stored into {@link #results} which is used to compose the hash of a root {@link File} at the
 * end of process.
 * <p>
 * It is recommended to use this version when you have RAID storage or your disc is fast enough to maintain
 * reading from multiple threads.
 * <p>
 * Reading the file tree happens from a single thread as well as composing the resulting hash. Hashing a file or a byte
 * array is done by multiple threads in parallel.
 * <p>
 * There are two modes supported: {@link Mode#HASHBYTES} and {@link Mode#HASHTHROUGH} which change the approach for
 * hashing. When {@link Mode#HASHBYTES} is chosen, all files from a given directory are supposed to be read into a memory
 * and than hashed by {@link #hashBytes(byte[])} in parallel. <b>Do not use this mode if there are no good reasons for that.</b>
 * When {@link Mode#HASHTHROUGH} is chosen, all files from a given directory are supposed to be hashed by
 * {@link #hashFile(File)} in parallel. <b>The most encouraged option to use.</b>
 * <p>
 * To compose the hash of a root file another traversal is done through the whole file tree starting from root. It is needed,
 * because {@link #results} contain hashes of particular files which should be correctly concatenated and additionally
 * hashed according to the algorithm.
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/22/2017
 * Time: 4:26 PM
 */
public class ProducerConsumerDirectoryHasher extends AsyncDirectoryHasher {

    public static final int DEFAULT_QUEUE_CAPACITY = 2048;
    public static final int DEFAULT_THREADS = Runtime.getRuntime().availableProcessors();

    private static final Logger LOG = LoggerFactory.getLogger(ProducerConsumerDirectoryHasher.class);

    private final BlockingQueue<HashingTask> taskQueue;
    private final Map<File, String> results = new ConcurrentHashMap<>();
    private final Mode mode;

    private ExecutorService executorService;
    private CountDownLatch finishedThreadsLatch;

    public ProducerConsumerDirectoryHasher(final Hasher fileHasher, final Mode mode) {
        this(fileHasher, mode, DEFAULT_THREADS, DEFAULT_QUEUE_CAPACITY);
    }

    public ProducerConsumerDirectoryHasher(final Hasher fileHasher, final Mode mode, final int threads, final int queueCapacity) {
        super(threads, queueCapacity, fileHasher);
        this.mode = mode;
        taskQueue = new ArrayBlockingQueue<>(queueCapacity);
    }

    /**
     * Implementation of an algorithm for hashing <u>directories</u> or particular files.
     *
     * Submits hashing tasks for every file in a tree under a {@code root} into {@link #taskQueue} and starts consumers
     * performing those tasks asynchronously. In the end all file hashes are composed into the final hash.
     *
     * @param root file or directory to be hashed
     * @return {@link String} representation of hexed hash of {@link File} contents
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    @Override
    public String hash(final File root) {
        initExecutorService();
        try {
            startHashingAsynchronously();
            putTasksRecursively(root);
            return processHashesRecursively(root);
        } finally {
            shutdownAndAwaitTermination();
        }
    }

    /**
     * Initiates a {@link ThreadPoolExecutor} with a specified number of threads and is backed by unlimited {@link LinkedBlockingQueue}
     *
     * @return {@link ExecutorService} to hold "consumers" which are responsible for hashing particular files
     */
    @Override
    protected ExecutorService initThreadPoolExecutor() {
        return Executors.newFixedThreadPool(threads);
    }

    private void startHashingAsynchronously() {
        finishedThreadsLatch = new CountDownLatch(threads);
        results.clear();
        IntStream.range(0, threads)
                .forEach(i -> executorService.submit(() -> {
                    for (; ; ) {
                        final HashingTask hashingTask = takeTask();
                        if (hashingTask instanceof PoisonPill) break;
                        final String hash = hashingTask.get();
                        results.put(hashingTask.file, hash);
                    }
                    finishedThreadsLatch.countDown();
                }));
    }

    private HashingTask takeTask() {
        try {
            return taskQueue.take();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new HashingException("Unable to hash a file", e);
        }
    }

    private void putTasksRecursively(final File root) {
        validate(root);
        if (!root.isFile() && !root.isDirectory()) throw new HashingException("You can hash only directory or root");
        if (root.isFile()) {
            putHashingTask(root, mode);
        } else if (root.isDirectory()) {
            putDirectoryTask(root);
        }
    }

    private void putHashBytesTask(final File file) {
        try {
            final byte[] bytes = readBytes(file);
            taskQueue.put(new HashingTask(file, bytes));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new HashingException("Unable to read a file " + file, e);
        } catch (Exception e) {
            throw new HashingException("Unable to read a file " + file, e);
        }
    }

    private void putHashFileTask(final File file) {
        try {
            taskQueue.put(new HashingTask(file));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new HashingException("Unable to read a file " + file, e);
        }
    }

    private void putHashingTask(final File file, final Mode mode) {
        switch (mode) {
            case HASHTHROUGH:
                putHashFileTask(file);
                break;
            case HASHBYTES:
                putHashBytesTask(file);
                break;
            default:
                throw new IllegalArgumentException("Unsupported mode: " + mode);
        }
    }

    private void putDirectoryTask(final File root) {
        getInternalFilesSortedByName(root).forEach(this::putTasksRecursively);
    }

    private String processHashesRecursively(final File root) {
        stopHashingAsynchronously();
        waitUntilThreadsAreDone();
        return collectHashes(root);
    }

    private void stopHashingAsynchronously() {
        IntStream.range(0, threads)
                .forEach(i -> {
                    try {
                        taskQueue.put(new PoisonPill());
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        throw new HashingException("Unable to stop hashing", e);
                    }
                });
    }

    private void waitUntilThreadsAreDone() {
        try {
            finishedThreadsLatch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new HashingException("Unable to process calculated hashes", e);
        }
    }

    private String collectHashes(final File root) {
        if (root.isFile()) {
            return results.get(root);
        }
        final String concatenatedInternalHashes = getInternalFilesSortedByName(root).stream()
                .map(this::collectHashes)
                .collect(joining());
        final String hash = hashString(concatenatedInternalHashes);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Hash of directory {} - {}", root, hash);
        }
        return hash;
    }

    private void initExecutorService() {
        executorService = initThreadPoolExecutor();
    }

    private void shutdownAndAwaitTermination() {
        shutdownAndAwaitTermination(executorService);
    }

    public enum Mode {
        HASHTHROUGH, HASHBYTES
    }

    private class HashingTask implements Supplier<String> {

        private final File file;
        private final byte[] bytes;

        private HashingTask(final File file, final byte[] bytes) {
            this.file = file;
            this.bytes = bytes;
        }

        private HashingTask(final File file) {
            this(file, null);
        }

        @Override
        public String get() {
            final String hash;
            if (bytes == null) {
                hash = hashFile(file);
            } else {
                hash = hashBytes(bytes);
            }
            if (LOG.isDebugEnabled()) {
                LOG.debug("Hash of {} - {}", file, hash);
            }
            return hash;
        }
    }

    private class PoisonPill extends HashingTask {
        private PoisonPill() {
            super(null, null);
        }
    }
}
