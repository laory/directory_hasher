package com.dbabichev.hashing_tool.hasher;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Abstract implementation of {@link DirectoryHasher} in order to provide a hasher with {@link ThreadPoolExecutor} facilities.
 * {@link ThreadPoolExecutor} uses {@link ArrayBlockingQueue} of a fixed {@link #queueCapacity} and handles
 * fixed number of {@link #threads}
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 12:34 PM
 */
public abstract class AsyncDirectoryHasher extends DirectoryHasher {

    protected final int threads;
    protected final int queueCapacity;

    protected AsyncDirectoryHasher(final int threads, final int queueCapacity, final Hasher fileHasher) {
        super(fileHasher);
        this.threads = threads;
        this.queueCapacity = queueCapacity;
    }

    /**
     * An abstract algorithm for hashing <u>directories</u> or particular files.
     *
     * @param file to be hashed
     * @return {@link String} representation of hexed hash of {@link File} content
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    @Override
    public abstract String hash(final File file);

    /**
     * Blocking method which is not applicable for async mode
     *
     * @throws UnsupportedOperationException when is called
     */
    @Override
    protected String hashDirectory(final File file) {
        throw new UnsupportedOperationException("Restricted in async mode");
    }

    /**
     * Method initiates the default {@link ThreadPoolExecutor} with {@link ArrayBlockingQueue} of a fixed {@link #queueCapacity} and
     * fixed number of {@link #threads}
     * <p>
     * Default {@link java.util.concurrent.RejectedExecutionHandler} is {@link ThreadPoolExecutor.CallerRunsPolicy} which
     * is used to not overload {@link ThreadPoolExecutor} when there are too many tasks submitted.
     *
     * @return {@link ThreadPoolExecutor} which is configured using supplied settings
     */
    protected ExecutorService initThreadPoolExecutor() {
        return new ThreadPoolExecutor(threads, threads,
                0L, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(queueCapacity),
                new ThreadPoolExecutor.CallerRunsPolicy());
    }


    /**
     * Shuts down given {@link ExecutorService} gracefully
     *
     * @param executorService to be shut down
     */
    protected void shutdownAndAwaitTermination(final ExecutorService executorService) {
        executorService.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!executorService.awaitTermination(60, TimeUnit.SECONDS))
                    throw new HashingException("Internal executor service was not terminated gracefully");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            executorService.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
            throw new HashingException("Internal executor service was not terminated gracefully", ie);
        }
    }
}
