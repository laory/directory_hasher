package com.dbabichev.hashing_tool.hasher;

/**
 * The main exception which should be wrapping all checked exceptions thrown by any implementation of {@link Hasher}.
 * Should be used for indicating some internal issue.
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/20/2017
 * Time: 7:08 PM
 */
public class HashingException extends RuntimeException {

    public HashingException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public HashingException(final String message) {
        super(message);
    }
}
