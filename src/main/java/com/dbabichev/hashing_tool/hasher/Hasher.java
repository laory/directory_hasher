package com.dbabichev.hashing_tool.hasher;

import java.io.File;

/**
 * A basic hasher interface which allows to use any algorithm or approach for hashing as well to abstract from a
 * vendor of third-party helper libraries.
 * <p>
 * The hash of a file is a {@link #hash(File)} of its contents.
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/21/2017
 * Time: 11:18 AM
 */
public interface Hasher {

    /**
     * Method calculates a hash of a {@link File} and transforms it into a string representation of a hex value.
     * <p>
     * The hash of a file is a hash of its contents then hexed encoded.
     *
     * @param file to be hashed
     * @return {@link String} representation of hexed hash of {@link File} content
     * @throws HashingException in case if given {@link File} can not be hashed due to internal issue
     */
    String hash(final File file);

    /**
     * Method calculates a hash of a string and transforms it into a string representation of a hex value
     *
     * @param string to be hashed
     * @return {@link String} representation of hexed hash of a string
     * @throws HashingException in case if given string can not be hashed due to internal issue
     */
    String hashString(final String string);

    /**
     * Method calculates a hash of a byte array and transforms it into a string representation of a hex value
     *
     * @param bytes array to be hashed
     * @return {@link String} representation of hexed hash of a byte array
     * @throws HashingException in case if given byte array can not be hashed due to internal issue
     */
    String hashBytes(final byte[] bytes);

    /**
     * Method reads all bytes from a given {@link File}
     *
     * @param file to be read as bytes
     * @return byte array of file's content
     * @throws HashingException in case if given {@link File} can not be read as bytes due to internal issue
     */
    byte[] readBytes(final File file);

    /**
     * Basic validation of a given {@link File}
     *
     * @param file to be validated
     * @throws HashingException in case if given {@link File} is not valid
     */
    default void validate(final File file) {
        if (file == null) throw new HashingException("File shouldn't be null");
        if (!file.exists()) throw new HashingException("File " + file + " doesn't exist");
    }
}
