package com.dbabichev.hashing_tool;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.joran.spi.JoranException;
import com.dbabichev.hashing_tool.hasher.*;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.Arrays;

import static com.dbabichev.hashing_tool.HashingTool.HasherType.*;

/**
 * The main facade which is used to consolidate a logic for starting hashing process with different settings.
 * <p>
 * Is supposed to be run as a main class in the jar from command line.
 * <p>
 * User: Dmytro_Babichev
 * Date: 9/20/2017
 * Time: 6:57 PM
 */
public class HashingTool {

    private static final HasherType DEFAULT_MODE = SINGLE_THREAD;
    private static final int DEFAULT_QUEUE_CAPACITY = ProducerConsumerDirectoryHasher.DEFAULT_QUEUE_CAPACITY;
    private static final int DEFAULT_THREADS = ProducerConsumerDirectoryHasher.DEFAULT_THREADS;

    private static final Logger LOG = LoggerFactory.getLogger(HashingTool.class);

    private static final Options OPTIONS = new Options();
    private static final Option VERBOSE = new Option("v", "verbose", false, "Log the process to System.out");
    private static final Option THREADS = Option.builder("t").longOpt("threads").hasArg().type(PatternOptionBuilder.NUMBER_VALUE).argName("number [1..n]")
            .desc(String.format("How many threads/consumers/workers will be processing a path or file. Non-applicable for %s mode. Default is %s", SINGLE_THREAD.name(), DEFAULT_THREADS)).build();
    private static final Option CAPACITY = Option.builder("c").longOpt("capacity").hasArg().type(PatternOptionBuilder.NUMBER_VALUE).argName("size [1..n]")
            .desc(String.format("The capacity of a blocking queue used for holding parallel tasks. Non-applicable for %s mode. Default is %s", SINGLE_THREAD.name(), DEFAULT_QUEUE_CAPACITY)).build();
    private static final Option PATH = Option.builder("p").longOpt("path").hasArg().argName("path")
            .desc("A path to a file or directory to be hashed").required().build();
    private static final Option MODE = Option.builder("m").longOpt("mode").hasArg().argName("mode").hasArg()
            .desc(String.format("Choose a mode which fits your needs the most: %s. Default mode is %s. The most likely your choice will be between: %s",
                    Arrays.toString(HasherType.values()), DEFAULT_MODE.name(), Arrays.toString(new String[]{SINGLE_THREAD.name(), PRODUCER_CONSUMER_HASHTHROUGH.name(), FIXED_THREAD_POOL.name()}))).build();

    static {
        OPTIONS.addOption(PATH);
        OPTIONS.addOption(MODE);
        OPTIONS.addOption(VERBOSE);
        OPTIONS.addOption(THREADS);
        OPTIONS.addOption(CAPACITY);
    }

    public static void main(String[] args) {
        final DefaultParser parser = new DefaultParser();
        try {
            final CommandLine commandLine = parser.parse(OPTIONS, args);
            final String path = commandLine.getOptionValue(PATH.getOpt());
            final HasherType mode = commandLine.hasOption(MODE.getOpt()) ? HasherType.valueOf(commandLine.getOptionValue(MODE.getOpt())) : DEFAULT_MODE;
            final int threads = commandLine.hasOption(THREADS.getOpt()) ? ((Long) commandLine.getParsedOptionValue(THREADS.getOpt())).intValue() : DEFAULT_THREADS;
            final int capacity = commandLine.hasOption(CAPACITY.getOpt()) ? ((Long) commandLine.getParsedOptionValue(CAPACITY.getOpt())).intValue() : DEFAULT_QUEUE_CAPACITY;
            final boolean verbose = commandLine.hasOption(VERBOSE.getOpt());
            setVerbose(verbose);
            if (capacity <= 0) {
                throw new IllegalArgumentException("Capacity must be bigger than 0");
            }
            if (threads <= 0) {
                throw new IllegalArgumentException("Number of threads must be bigger than 0");
            }
            final File file = new File(path);
            final Hasher directoryHasher = getHasher(mode, threads, capacity);

            final long start = System.currentTimeMillis();

            final String hash = directoryHasher.hash(file);

            final long finish = System.currentTimeMillis() - start;

            System.out.println(hash);

            if (LOG.isDebugEnabled()) {
                LOG.debug("Elapsed time: {}ms", finish);
            }
        } catch (ParseException e) {
            e.printStackTrace(System.err);
            new HelpFormatter().printHelp("java -jar directory_hasher-with-dependencies-1.0-SNAPSHOT.jar", OPTIONS, true);
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    private static Hasher getHasher(final HasherType hasherType, final int threads, final int capacity) {
        final Hasher fileHasher = new GuavaFileHasher();
        switch (hasherType) {
            case FIXED_THREAD_POOL:
                return new FixedThreadPoolDirectoryHasher(fileHasher, threads, capacity);
            case PRODUCER_CONSUMER_HASHTHROUGH:
                return new ProducerConsumerDirectoryHasher(fileHasher, ProducerConsumerDirectoryHasher.Mode.HASHTHROUGH, threads, capacity);
            case PRODUCER_CONSUMER_HASHBYTES:
                return new ProducerConsumerDirectoryHasher(fileHasher, ProducerConsumerDirectoryHasher.Mode.HASHBYTES, threads, capacity);
            case FORK_JOIN:
                return new ForkJoinDirectoryHasher(fileHasher, threads);
            case SINGLE_THREAD:
                return new SingleThreadDirectoryHasher(fileHasher);
        }
        throw new IllegalArgumentException("Unsupported hasher type: " + hasherType);
    }

    /**
     * Enumeration of available HashingTool modes
     */
    public enum HasherType {
        SINGLE_THREAD("Use when reading from disc is a bottle-neck"),
        PRODUCER_CONSUMER_HASHTHROUGH("Producer reads a directory tree from a single thread and distributes the work of hashing files between consumers. Every consumer hashes a file without reading the whole file into the memory. Efficient for RAID data storage or when you have a lot of regular files"),
        PRODUCER_CONSUMER_HASHBYTES("Producer reads every file in directory from a single thread and distributes the work of hashing byte arrays between consumers. This mode requires a huge heap size to keep whole files in memory. Experimental mode, use with care"),
        FORK_JOIN("Directory is recursively hashed by workers in a work-stealing fashion. Use this mode only when reading from disc is extremely-fast and can be treated as non-blocking, otherwise the process will be even slower than SINGLE_THREAD mode"),
        FIXED_THREAD_POOL("Every file and directory is hashed in a separate CompletableFuture which is invoked on FixedThreadPoolExecutorService which is backed by an ArrayBlockingQueue. May be efficient when hashing relatively flat directory with files of medium size");

        private final String description;

        HasherType(final String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return name() + ": " + description;
        }
    }

    private static void setVerbose(final boolean verbose) throws JoranException {
        System.setProperty("verbose", Boolean.toString(verbose));
        reloadLoggingConfiguration();
    }

    private static void reloadLoggingConfiguration() throws JoranException {
        final LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        final ContextInitializer ci = new ContextInitializer(loggerContext);
        final URL url = ci.findURLOfDefaultConfigurationFile(true);
        loggerContext.reset();
        ci.configureByResource(url);
    }
}
